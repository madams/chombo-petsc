#ifdef CH_LANG_CC
/*
 *      _______              __
 *     / ___/ /  ___  __ _  / /  ___
 *    / /__/ _ \/ _ \/  V \/ _ \/ _ \
 *    \___/_//_/\___/_/_/_/_.__/\___/
 *    Please refer to Copyright.txt, in Chombo's root directory.
 */
#endif
#ifdef CH_USE_PETSC
#include "PetscCompGrid.H"

#include "NamespaceHeader.H"

std::ostream& operator<< (std::ostream& os, GID_type a_type)
{
  switch (a_type)
    {
    case GHOST: os << "ghost";                       break;
    case FINE_COVERED: os << "covered (with fine)" ; break;
    case DIRI_BC: os << "BC (Diri)";                 break;
    case NEUM_BC: os << "BC (Neum)";                 break;
    case ANY_BC: os << "BC";                         break;
    case UNKNOWN: os << "unknown";                   break;
    default: os << "index " << (int)a_type;          break;
    }
  return os;
}

//
// My BC function
//
CompBC::~CompBC()
{
  PetscFree(m_Rcoefs);
}

CompBC::CompBC(int a_order, IntVect a_nGhosts) : m_Rcoefs(0), m_isDefined(false)
{
  define(a_order,a_nGhosts);
}

void 
CompBC::define(int a_order, IntVect a_nGhosts)
{  
  if (m_Rcoefs) PetscFree(m_Rcoefs);
  if (a_nGhosts[0]!=1 && a_nGhosts[0]!=2)
    MayDay::Error("Unsupported number of ghosts in CompBC");
  
  m_nGhosts = a_nGhosts; m_nSources = a_order+1;
  PetscMalloc(m_nSources*m_nGhosts[0]*sizeof(PetscReal),&m_Rcoefs);

  m_isDefined = false; // needs to be set
}

PetscReal CompBC::getCoef(int a_iSrc, int a_iGhost)
{
  if (!m_isDefined) createCoefs();
  return m_Rcoefs[a_iGhost*m_nSources + a_iSrc];
}
//
void 
ConstDiriBC::createCoefs()
{  
  m_isDefined=true;

  if (m_nSources==1) 
    {
      m_Rcoefs[0] = -1.;
      if (m_nGhosts[0]==2) m_Rcoefs[1] = -3.;	
    }
  else if (m_nSources==2) 
    { // s = 6, 18
      m_Rcoefs[0] = -5./2; m_Rcoefs[1] = 1./2;
      if (m_nGhosts[0]==2) 
	{
	  m_Rcoefs[2] = -21./2; m_Rcoefs[3] = 5./2;
	}
    }
  else if (m_nSources==3) 
    { // s = 12, 48
      m_Rcoefs[0] = -13./3; m_Rcoefs[1] = 5./3; m_Rcoefs[2] = -1./3;
      if (m_nGhosts[0]==2) 
	{
	  m_Rcoefs[3] = -70./3; m_Rcoefs[4] = 32./3;  m_Rcoefs[5] = -7./3; 
	}
    }
  else if (m_nSources==4) 
    { // s = 60, 300
      m_Rcoefs[0] = -77./12; m_Rcoefs[1] = 43./12; m_Rcoefs[2] = -17./12; m_Rcoefs[3] = 3./12;
      if (m_nGhosts[0]==2) 
	{
	  m_Rcoefs[4] = -505./12; m_Rcoefs[5] = 335./12;  m_Rcoefs[6] = -145./12; m_Rcoefs[7] = 27./12; 
	}
    }
  else
    MayDay::Error("Unsupported degree in ConstDiriBC");
}

void 
ConstDiriBC::operator()( FArrayBox&           a_state,
			 const Box&           a_valid,
			 const ProblemDomain& a_domain,
			 Real                 a_dx,
			 bool                 a_homogeneous)
{
  const Box& domainBox = a_domain.domainBox();
  
  for (int idir = 0; idir < SpaceDim; idir++)
    {
      if (!a_domain.isPeriodic(idir))
	{
	  for (SideIterator sit; sit.ok(); ++sit)
	    {
	      Side::LoHiSide side = sit();		
	      if (a_valid.sideEnd(side)[idir] == domainBox.sideEnd(side)[idir])
		{
		  // Dirichlet BC
		  int isign = sign(side);
		  Box toRegion = adjCellBox(a_valid, idir, side, 1);
		  toRegion &= a_state.box();
		  for (BoxIterator bit(toRegion); bit.ok(); ++bit)
		    {
		      IntVect ivTo = bit();			
		      IntVect ivClose = ivTo - isign*BASISV(idir);
		      for (int ighost=0;ighost<m_nGhosts[0];ighost++,ivTo += isign*BASISV(idir))
			{
			  for (int icomp = 0; icomp < a_state.nComp(); icomp++) a_state(ivTo, icomp) = 0.0;
			  IntVect ivFrom = ivClose;
			  for (int i=0;i<m_nSources;i++,ivFrom -= isign*BASISV(idir))
			    {
			      for (int icomp = 0; icomp < a_state.nComp(); icomp++)
				{
				  a_state(ivTo, icomp) += m_Rcoefs[ighost*m_nSources + i]*a_state(ivFrom, icomp);
				}
			    }
			}
		    }
		} // if ends match
	    } // end loop over sides
	} // if not periodic in this direction
    } // end loop over directions    
}

#include "NamespaceFooter.H"
#endif
