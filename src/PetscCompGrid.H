#ifdef CH_LANG_CC
/*
 *      _______              __
 *     / ___/ /  ___  __ _  / /  ___
 *    / /__/ _ \/ _ \/  V \/ _ \/ _ \
 *    \___/_//_/\___/_/_/_/_.__/\___/
 *    Please refer to Copyright.txt, in Chombo's root directory.
 */
#endif

#ifndef _PETSCCOMPGRID_H_
#define _PETSCCOMPGRID_H_
#ifdef CH_USE_PETSC
#include "Stencil.H"
#include "BCFunc.H"
#include "DisjointBoxLayout.H"
#include "BoxIterator.H"
#include "FourthOrderInterpStencil.H"
#include <petsc-private/pcimpl.h>
#include "NamespaceHeader.H"

typedef enum {GHOST=-1,FINE_COVERED=-2,DIRI_BC=-3,
	      NEUM_BC=-4,ANY_BC=-5,UNKNOWN=-6} GID_type; 

std::ostream& operator<< (std::ostream& os, GID_type);

//! \class PetscCompGrid
//! This base class organizes the construction of a PETSc matrix, with an AMR hierarchy
template<int DOF>
class PetscCompGrid
{
  //friend class PetscAMRSolver;
public:
  typedef std::pair<IndexML,StencilValue<DOF> > StenMatNode;
  typedef std::pair<IndexML,StencilValue<1> > StenScaleNode;
  typedef std::map<IndexML,StencilValue<DOF> > StencilMat;
  typedef std::map<IndexML,StencilValue<1> > StencilScale;
  
  //! Base class constructor. Called by all subclass constructors.
  PetscCompGrid() : m_gid0(0), m_mat(0), m_maxStencilDist(2),m_writeMatlab(false),m_verbose(0)
  {
  }
  
  virtual void define( const ProblemDomain &a_cdomain,
		       Vector<DisjointBoxLayout> &a_grids, 
		       Vector<int> &a_refratios, 
		       BCHolder a_bc,
		       const RealVect &a_cdx,
		       int a_ibase=0, int a_maxLev=-1);
  
  //! Destructor.
  virtual ~PetscCompGrid();
  virtual void clean();

  Mat getMatrix() const { return m_mat; }
  void setMatlab(bool b=true) {m_writeMatlab = b;}
  void setVerbose(int a_v){ m_verbose=a_v;}

  // main Mat creation routines
  PetscErrorCode createMatrix();
  PetscErrorCode putChomboInPetsc( const Vector<LevelData<FArrayBox> * > &rhs, Vec b )const;
  PetscErrorCode putPetscInChombo( Vec b, Vector<LevelData<FArrayBox> * > &rhs )const;
  virtual IntVect getGhostVect()const = 0;
  IntVect getCFStencil(const ProblemDomain &a_dom, const IntVect a_ivc);
protected:
  virtual PetscErrorCode createOpStencil(IntVect,int,DataIterator, StencilMat &) = 0;
  virtual PetscErrorCode applyBCs(IntVect,int,DataIterator,Box,StencilMat &);
  virtual PetscErrorCode InterpToFine(IntVect,int,DataIterator,StencilMat &);
  virtual PetscErrorCode InterpToCoarse(IntVect,int,DataIterator,StencilMat &);
  PetscErrorCode AddStencil(IntVect,int,DataIterator,StencilMat &);
  PetscErrorCode projectStencil( IndexML jiv, Vector<StenScaleNode> &a_newNodes, 
				 StencilMat &a_sten);
  // utils
  void defineNode(IntVect a_iv, int a_lev, Real a_val, StenScaleNode &a_node)
  {
    a_node.first.setIV(a_iv);
    a_node.first.setLevel(a_lev);
    a_node.second.setValue(a_val);
  }

  // data
  Vector<ProblemDomain> m_domains;
  Vector<DisjointBoxLayout> m_grids;
  Vector<int> m_refRatios;
  Vector<RealVect> m_dxs;
public:
  Vector<RefCountedPtr<LevelData<BaseFab<PetscInt> > > > m_GIDs;
protected:
  Vector<RefCountedPtr<LevelData<BaseFab<PetscInt> > > > m_crsSupportGIDs;
  Vector<RefCountedPtr<LevelData<BaseFab<PetscInt> > > > m_fineCoverGIDs;
public:
  int m_gid0;
protected:
  Mat m_mat;
  BCHolder m_bc;
  int m_maxStencilDist;
  /// all possible stencils, on (-m_maxStencilDist:+m_maxStencilDist)^SpaceDim
  BaseFab<FourthOrderInterpStencil*> m_FCStencils;
  bool m_writeMatlab;
  int m_verbose;
private:
  // Disallowed for all the usual reasons
  void operator=(const PetscCompGrid& a_input)
  {
    MayDay::Error("invalid operator");
  }
  // Disallowed for all the usual reasons
  PetscCompGrid(const PetscCompGrid& a_input)
  {
    MayDay::Error("invalid operator");
  }
};

class CompBC //: public BCFunction
{
public:
  CompBC(int a_nSource, IntVect a_nGhosts);
  ~CompBC();
  void define(int a_nSource, IntVect a_nGhosts);

  virtual void createCoefs() = 0;
  virtual void operator()( FArrayBox&           a_state,
			   const Box&           a_valid,
			   const ProblemDomain& a_domain,
			   Real                 a_dx,
			   bool                 a_homogeneous) = 0;
 
  IntVect nGhosts()const{return m_nGhosts;}
  int nSources()const{return m_nSources;}
  PetscReal getCoef(int a_iSrc, int a_iGhost=0);
protected:
  PetscReal *m_Rcoefs;
  IntVect m_nGhosts;
  int m_nSources; // degreee is m_nSources-1
  bool m_isDefined;
};

class ConstDiriBC : public CompBC
{
public:
  ConstDiriBC(int a_nSource=1, IntVect a_nGhosts=IntVect::Unit) : CompBC(a_nSource,a_nGhosts) {}
  virtual void createCoefs();
  virtual void operator()( FArrayBox&           a_state,
			   const Box&           a_valid,
			   const ProblemDomain& a_domain,
			   Real                 a_dx,
			   bool                 a_homogeneous);
};
#include "NamespaceFooter.H"

#ifndef CH_EXPLICIT_TEMPLATES
#include "PetscCompGridI.H"
#endif // CH_EXPLICIT_TEMPLATES

#endif
#endif
