#ifdef CH_LANG_CC
/*
 *      _______              __
 *     / ___/ /  ___  __ _  / /  ___
 *    / /__/ _ \/ _ \/  V \/ _ \/ _ \
 *    \___/_//_/\___/_/_/_/_.__/\___/
 *    Please refer to Copyright.txt, in Chombo's root directory.
 */
#endif

#include "Stencil.H"

// IndexML:operator<<
ostream&
operator<< (ostream&       os,
            const IndexML& p)
{
  os << '(' << p.m_iv << ',' << p.level() << ')';
  if (os.fail())
    MayDay::Error("operator<<(ostream&,IndexML&) failed");
  return os;
}
